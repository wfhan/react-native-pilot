import React, {Component} from 'react';
import {StyleSheet, TabBarIOS, View, Text} from 'react-native';
import Featured from './Featured';
import Search from './Search';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
    // alignItems: 'center'
  }
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'featured'
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TabBarIOS selectedTab={this.state.selectedTab}>
          <TabBarIOS.Item
            selected={this.state.selectedTab === 'featured'}
            title="Featured"
            systemIcon='featured'
            onPress={() => {
              this.setState({
                selectedTab: 'featured'
              });
            }}>
            <Featured/>
          </TabBarIOS.Item>
          <TabBarIOS.Item
            selected={this.state.selectedTab === 'search'}
            title="Search"
            systemIcon='search'
            onPress={() => {
              this.setState({
                selectedTab: 'search'
              });
            }}>
            <Search/>
          </TabBarIOS.Item>
        </TabBarIOS>
      </View>
    );
  }
}
