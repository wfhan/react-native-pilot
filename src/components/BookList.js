import React, {Component} from 'react';
import {StyleSheet, View, Text, Image, ListView, TouchableHighlight, ActivityIndicatorIOS} from 'react-native';
import {observer} from 'mobx-react/native';

import BookDetail from './BookDetail'
import BookStore from '../stores/BookStore';

const REQUEST_URL = 'https://www.googleapis.com/books/v1/volumes?q=subject:fiction';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    padding: 10
  },
  thumbnail: {
    width: 53,
    height: 81,
    marginRight: 10,
  },
  rightContainer: {
    flex: 1
  },
  title: {
    fontSize: 20,
    marginBottom: 8
  },
  author: {
    color: '#656565'
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    backgroundColor: '#F5FCFF'
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

@observer
export default class BookList extends Component {
  static defaultProps = {
    bookStore: BookStore
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2
      })
    };
  }

  componentDidMount() {
    let self = this;
    BookStore.getFeaturedBooks().done(() => { 
      self.setState({ isLoading: false });
    });
  }

  showBookDetail(book) {
    this.props.navigator.push({
      title: book.volumeInfo.title,
      component: BookDetail,
      passProps: {book},
      leftButtonTitle: 'Back',
      onLeftButtonPress: this.props.navigator.pop
    });
  }

  renderBook(book) {
    return (
      <TouchableHighlight onPress={() => this.showBookDetail(book)}  underlayColor='#dddddd'>
        <View>
          <View style={styles.container}>
            <Image
              source={{uri: book.volumeInfo.imageLinks.thumbnail}}
              style={styles.thumbnail} />
            <View style={styles.rightContainer}>
              <Text style={styles.title}>{book.volumeInfo.title}</Text>
              <Text style={styles.author}>{book.volumeInfo.authors}</Text>
            </View>
          </View>
          <View style={styles.separator} />
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    let dataSource = this.state.dataSource.cloneWithRows(BookStore.books.slice());
    if (this.state.isLoading) {
      return (
        <View style={styles.loading}>
          <Text>Loading books...</Text>
        </View>
      );
    }

    return (
      <ListView
        dataSource={dataSource}
        renderRow={this.renderBook.bind(this)}
        style={styles.listView}
      />
    );
  }
}