import React, {Component} from 'react';
import {StyleSheet, View, Text, TextInput, TouchableHighlight, ActivityIndicatorIOS} from 'react-native';

import SearchResults from './SearchResults';
import BookStore from '../stores/BookStore';

const styles = StyleSheet.create({
    container: {
        marginTop: 65,
        padding: 10
    },
    searchInput: {
        marginTop: 5,
        marginBottom: 10,
        // fontSize: 18,
        borderWidth: 0.5,
        flex: 1,
        borderRadius: 2,
        padding: 5
    },
    button: {
        height: 36,
        backgroundColor: '#f39c12',
        borderRadius: 8,
        justifyContent: 'center',
        marginTop: 15
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    instructions: {
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 15
    },
    fieldLabel: {
        fontSize: 15,
        marginTop: 15
    },
    errorMessage: {
        fontSize: 15,
        alignSelf: 'center',
        marginTop: 15,
        color: 'red'
    }
});

export default class SearchBook extends Component {
	constructor(props) {
    super(props);
    this.state = {
      bookAuthor: '',
      bookTitle: '',
      isLoading: false,
      errorMessage: ''
    };
  }

  bookTitleInput(event) {
    this.setState({ bookTitle: event.nativeEvent.text });
  }

  bookAuthorInput(event) {
    this.setState({ bookAuthor: event.nativeEvent.text });
  }

  searchBooks() {
    let self = this;
    let { bookTitle, bookAuthor } = this.state;
    this.setState({ isLoading: true });

    BookStore.searchBooks(bookTitle, bookAuthor).catch(err => {
      this.setState({errorMessage: err});
    }).done(() => {
      self.setState({ isLoading: false });
      self.props.navigator.push({
        title: 'Search Results',
        component: SearchResults,
        passProps: { books: BookStore.books.slice() },
        leftButtonTitle: 'Back',
        onLeftButtonPress: self.props.navigator.pop
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.instructions}>Search by book title and/or author</Text>
        <View style={{ height:80 }}>
          <Text style={styles.fieldLabel}>Book Title:</Text>
          <TextInput style={styles.searchInput} onChange={this.bookTitleInput.bind(this)}/>
        </View>
        <View style={{ height:80 }}>
          <Text style={styles.fieldLabel}>Author:</Text>
          <TextInput style={styles.searchInput} onChange={this.bookAuthorInput.bind(this)}/>
        </View>
        { 
          !this.state.isLoading && 
          <TouchableHighlight style={styles.button} underlayColor='#f1c40f' onPress={this.searchBooks.bind(this)}>
            <Text style={styles.buttonText}>Search</Text>
          </TouchableHighlight>
        }
        { 
          this.state.isLoading && 
          <TouchableHighlight style={styles.button} underlayColor='#f1c40f'>
            <Text style={styles.buttonText}>Searching</Text>
          </TouchableHighlight>
        }
        <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
      </View>
    );
  }
}